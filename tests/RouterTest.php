<?php

include('../vendor/autoload.php');

use Fwepe\Component\Uri\Blueprint;
use Fwepe\Component\Uri\RouteNode;

class RouterTest extends PHPUnit_Framework_TestCase
{
    private $route;
    private $blueprint;

    public function setUp()
    {
        $this->route = "/news/{2:id}/{1:name}";
        $this->blueprint = new Blueprint("/news/{2:id}/{1:name}",
                'Fwepe\\Controller\\Home::index');
    }
    
    public function testRouteNodeWithName()
    {
        $node = new RouteNode();
        
        $node->setIndex(1);
        $node->setName("baba");

        $this->assertEquals("{1:baba}", $node->toString());
    }
    
    public function testRouteNodeWithoutName()
    {
        $node = new RouteNode();
        
        $node->setIndex(1);

        $this->assertEquals("{1}", $node->toString());
    }
    
    public function testBlueprintWithName()
    {
        $blueprint = new Blueprint("/news/{2:id}/{1:name}",
                'Fwepe\\Controller\\Home::index');
        
        $expectedTarget = array(
            "class" => 'Fwepe\\Controller\\Home',
            'method' => 'index'
        );

        $this->assertEquals($expectedTarget, $blueprint->getTarget());
        
        $this->assertEquals(
            "~^/news/([\w-]+)/([\w-]+)$~",
            $blueprint->getRouteRegex()
        );
        
        $path = "/news/5/people-explode";

        
        $this->assertSame(array(
            "people-explode", "5"
        ), $blueprint->getParams($path));
        
        // Test reverse URL with key name
        $this->assertEquals(
            "/news/6/abc",
            $blueprint->reverseUrl(array("id" => 6, "name" => "abc"))
        );
        
        // Test reverse URL with key index
        $this->assertEquals(
            "/news/6/abc",
            $blueprint->reverseUrl(array("abcd", "6"))
        );
    }
    
    public function testBlueprintWithoutName()
    {
        $blueprint = new Blueprint("/news/{2}/{1}",
                'Fwepe\\Controller\\Home::index');
        
        $expectedTarget = array(
            "class" => 'Fwepe\\Controller\\Home',
            'method' => 'index'
        );
                
        $this->assertEquals($expectedTarget, $blueprint->getTarget());
        
        $this->assertEquals(
            "~^/news/([\w-]+)/([\w-]+)$~",
            $blueprint->getRouteRegex()
        );
        
        $path = "/news/5/people-explode";
        
        $pathValues = $blueprint->extractPathValues($path);
        
        $this->assertSame(array("5", "people-explode"), $pathValues);
        
        $this->assertSame(array(
            "people-explode", "5"
        ), $blueprint->getParams($path));
        
        // Test reverse URL with key index
        $this->assertEquals(
            "/news/6/abc",
            $blueprint->reverseUrl(array("abc", "6"))
        );
    }
    
}