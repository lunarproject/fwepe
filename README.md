FWEPE
===================================

This is a simple PHP framework


Installation
-----------------------------------

You need to install [Composer], then execute this commands:

    composer create-project -s dev lunarproject/fwepe-project <app_name>
    
[Composer]: http://getcomposer.org