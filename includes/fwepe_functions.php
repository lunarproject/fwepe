<?php

/*
 * Copyright (c) 2012 Farly <farly@lunarproject.org>
 *
 * This file is part of FWEPE 3.
 *
 * FWEPE 3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FWEPE 3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FWEPE 3.  If not, see <http ://www.gnu.org/licenses/>.
 */

if(!function_exists('print_pre')) {
    function print_pre($varArray)
    {
        echo '<pre>';print_r($varArray);echo '</pre>';
    }
}

if(!function_exists('instance_get')) {
    function &instance_get()
    {
        return Fwepe\Factory\MVC\Controller::getInstance();
    }
}

if(!function_exists('config_get')){
    function config_get($config, $key, $default = '')
    {
        $config = new Fwepe\Factory\Config($config);
        return $config->get($key, $default);
    }
}

if(!function_exists('url_get'))
{
    function url_get()
    {
        $url = trim(config_get('Common', 'URL'), '/');
        return $url;

    }
}

if(!function_exists('href_get')) {
    function href_get($pathInfo = null, $get = null)
    {
        if(is_array($pathInfo)) {
            $pathInfo = implode('/', $pathInfo);
        } else {
            $pathInfo = trim($pathInfo, '/');
        }

        if(\Fwepe\Component\Uri\Dispatcher::isRegistered('PathInfo'))
            $pathInfo = \Fwepe\Component\Uri\Dispatcher::fetch('PathInfo', $pathInfo);

        if(is_array($get)) {
            $gets = array();

            foreach($get as $key => $value) {
                $gets[] = $key . '=' . $value;
            }

            $gets = '?' . implode('&', $gets);
        } else {
            $gets = '';
            if($get !== null)
            {
                $gets = '?' . $get;
            }
        }

        $frontUrl = config_get('Common', 'FrontUrl');
        if($frontUrl !== '') {
            $frontUrl .= '/';
        } else {
            $frontUrl = null;
        }

        $suffix = config_get('Common', 'UrlSuffix');
        if(!empty($suffix)) {
            $suffix = '.' . $suffix;
        }

        return url_get() . '/' . $frontUrl . $pathInfo . $gets . $suffix;
    }
}

if(!function_exists('href_echo')) {
	function href_echo()
	{
		$args = func_get_args();
		
		echo call_user_func_array('href_get', $args);
		
	}
}
if(!function_exists('vendor_include')) {
    function vendor_include($file)
    {
        static $loadedVendor;

        if(!in_array($file, (array) $loadedVendor)) {
            $loadedVendor[] = $file;
            $file = str_replace('/', DS, $file);
            $file = str_replace('\\', DS, $file);
            $file = trim($file, DS);

            if(is_file(PATH_APP .DS. "Vendor" .DS. $file)) {
                require_once(PATH_APP .DS. "Vendor" .DS. $file);
			} else if(is_file(PATH_ROOT .DS. "vendor" .DS. $file)) {
                require_once(PATH_ROOT .DS. "vendor" .DS. $file);
			}
        }
    }
}

if(!function_exists('array_empty')) {
    function array_empty($mixed)
    {
        if (is_array($mixed)) {
            foreach ($mixed as $value) {
                if (!array_empty($value)) {
                    return false;
                }
            }
        } else if (!empty($mixed)) {
            return false;
        }
        return true;
    }
}


if(!function_exists('array_is_empty')) {
    function array_is_empty($mixed)
    {
        if (is_array($mixed)) {
            foreach ($mixed as $value) {
                if (array_empty($value)) {
                    return true;
                }
            }
        } else if (empty($mixed)) {
            return true;
        }
        return false;
    }
}

if(!function_exists('array_clear'))
{
	function array_clear(&$array)
	{
		foreach($array as $key => $value)
		{
			$array[$key] = null;
		}
	}
}

if(!function_exists('array_value_empty')) {
	function array_value_empty($array)
	{
		$isEmpty = false;
		foreach($array as $values) {
			if(empty($values)) {
				$isEmpty = true;
				break;
			}
		}

		return $isEmpty;
	}
}

if(!function_exists('__')) {
    function __($text)
    {
        static $translator;
        $useTranslator = config_get('Translator', 'LoadTranslator', false);
        if($useTranslator === true) {
            if(!isset($translator)) {
                $translator = new \Fwepe\Factory\Translator();
            }
            $args = func_get_args();
            $args[0] = $translator->translate($args[0]);
            return call_user_func_array('sprintf', $args);
        } else {
            return $text;
        }
    }
}

if(!function_exists('_e')) {
    function _e($text)
    {
        echo __($text);
    }
}

if(!function_exists('cookie_set')) {

    function cookie_set($name, $value, $expire = 0, $path = PATH_APP, $domain = null, $secure = false, $http_only = false)
    {
        setcookie($name, $value, $expire, $path, $domain, $secure, $http_only);
    }
}


if(!function_exists('cookie_get')) {
    function cookie_get($key)
    {
        if(isset($_COOKIE[$key]))
            return $_COOKIE[$key];
        else
            return null;
    }
}


if(!function_exists('jump')) {
    function jump($uri, $delay = 0)
    {
        if($delay === 0)
        {
            header("location:$uri");
        }
        else if($delay > 0)
        {
            header("Refresh:$delay; URL=$uri");
        }
    }
}

if(!function_exists('encrypt')) {
    function encrypt($string)
    {
        $token = config_get("Security", "EncryptToken");
        return md5($token.$string);
    }
}

if(!function_exists('ds_clean')) {

    function ds_clean($string)
    {
        $string = str_replace('/', DS, $string);
        $string = str_replace('\\', DS, $string);

        return $string;
    }
}

if(!function_exists('create_empty_array')) {
	function create_empty_array($array = array())
	{
		$out = array();
		
		foreach($array as $value) {
			$out[$value] = "";
		}
		
		return $out;
	}
}

if(!function_exists('generate_code')) {
	function generate_code($digit)
	{
		$code = '';
		for ($i = 0; $i < $digit; $i++) {
				$rand_val = rand(65, 100);
				if ($rand_val > 90) {

						$rand_val = $rand_val - 91;
						$code .= $rand_val;
				}
				else {
						$code .= chr($rand_val);
				}
		}

		return $code;
	}
}

if(!function_exists('url_for'))
{
    function url_for($name, array $params = array())
    {
        $routes = config_get('Router', 'Blueprint');
        
        foreach($routes as $route)
        {
            # TODO: This is bad way to link static files
            if($name == 'static')
            {
                return config_get('Common', 'StaticURL') .  ltrim($params[0], '/');
            }
            else
            {
                $routeName = $route[1];
                if(isset($route[2]))
                    $routeName = $route[2];

                if($name == $routeName)
                {
                    $blueprint = new \Fwepe\Component\Uri\Blueprint($route[0], $route[1]);
                    $blueprint->getRouteRegex();

                    return config_get('Common', 'URL') . $blueprint->reverseUrl($params);
                }
            }

        
        }
        
    }
}

/*** End: functions.php ***/
