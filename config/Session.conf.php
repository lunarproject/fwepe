<?php

$cfg['Name']             = 'FWEPE_SESSION';
$cfg['Token']            = 'FwepeToken';
$cfg['ActiveTime']       = 0; //0 = unlimited;
$cfg['CheckUserAgent']   = true;
$cfg['RefreshSessionId'] = 300;
$cfg['Prefix']           = 'fwepe_';

/*** End: Session.conf.php ***/
