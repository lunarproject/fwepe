<?php

$cfg["Autoload"] = array(
    "template" => new \Fwepe\Vendor\Twig(),
    new \Fwepe\Factory\Session(),
    new \Fwepe\Factory\Request()
);


$cfg["Plugins"] = array();
/*** End: Controller.conf.php ***/
