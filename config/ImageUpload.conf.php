<?php
$cfg['AllowedExtentions'] = array(
        'jpg'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'png'  => 'image/png',
        'gif'  => 'image/gif'
);

$cfg['MaxSize']      = 512000;
$cfg['Permission']   = 0775;
$cfg['Location']     = PATH_TMP;
$cfg['Pattern']      = ':name:_:counter:';
$cfg['CounterDigit'] = 2;
$cfg['Overwrite']    = false;

/*** End: ImageUpload.conf.php ***/
