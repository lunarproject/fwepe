<?php

$cfg['AllowedExtensions'] = array(
    //Archive
    'zip', '7z', 'rar', 'gz', 'tar', 'tgz',
    //Document
    'txt', 'pdf', 'doc', 'xls', 'ppt', 'odt', 'odf',
    //Image
    'gif', 'png', 'jpg', 'jpeg',
    //Audio
    'mp3', 'wav', 'mpeg', 'mpg', 'mpe', 'ogg',
    //Video
    'mov', 'avi', 'mkv', 'flv'
);

$cfg['MaxSize']      = 512000;
$cfg['Permission']   = 0775;
$cfg['Location']     = PATH_TMP;
$cfg['Pattern']      = ':name:_:counter:';
$cfg['CounterDigit'] = 2;
$cfg['Overwrite']    = false;

/*** End: Upload.conf.php ***/
