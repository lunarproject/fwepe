<?php

namespace Fwepe\Factory\MVC;

use Fwepe\Factory\Database\FPDO;

/**
 * Model class
 *
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 * @package Fwepe\Core
 */

class Model
{

    public function __construct()
    {
        $this->setDatabase();
    }

    public function setDatabase($key = "Default")
    {
        $dbClass = new FPDO($key);
        $this->db = $dbClass;
    }

}

/*** End: Model.php ***/
