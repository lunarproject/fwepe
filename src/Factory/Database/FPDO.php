<?php

namespace Fwepe\Factory\Database;

class FPDO extends \PDO
{
    public function __construct($dbKey = "Default")
    {
        $dbConfig = config_get("Database", "Databases");
        $db = $dbConfig[$dbKey];
        parent::__construct($db["dsn"], $db["user"], $db["pass"]);
    }
}

/*** End: FPDO.php ***/
