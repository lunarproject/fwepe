<?php

namespace Fwepe\Factory\Plugin;


use Fwepe\Component\Uri\Dispatcher as UriDispatcher;
//use Fwepe\Component\Uri\Router;
use Fwepe\Component\Plugin\FactoryPlugin;
use Fwepe\Factory\Config;
use Fwepe\Factory\I18n;

class MultiLingual extends FactoryPlugin
{
    public $id = "MultiLingual";
    public $enabled = false;
    public function __construct()
    {
        $this->config = new Config("Plugin/MultiLingual");
        $this->enabled = $this->config->get("Enable");
    }

    public function onConstruct()
    {
        if($this->config->get('LanguageMethod') == 'Uri') {

            $builder = $this->config->get('URIEngine');
            $name    = $this->config->get('LanguageIdentifier');
            $default = $this->config->get('DefaultLanguage');

            UriDispatcher::register($builder, $name, $default);

            $_SERVER['PATH_INFO'] = UriDispatcher::getOriginalUri($builder);

            $lang = UriDispatcher::get($builder, $name);
            I18n::setLanguage($lang);
            
        } else if($this->config->get('LanguageIdentifier') == 'Cookie') {
            $default = $this->config->get('DefaultLanguage');
            $name    = $this->config->get('LanguageIdentifier');
            if(cookie_get($name))
                $lang = $default;
            else
                $lang = cookie_get($name);

            I18n::setLanguage($lang);
        }
    }

    public function beforeBuild() {}
    public function afterBuild() {}

}
