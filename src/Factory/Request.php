<?php

namespace Fwepe\Factory;

use Fwepe\Component\Request\RequestManager;
use Fwepe\Factory\Security;

class Request extends RequestManager
{
    public function __construct()
    {
        $security = new Security();
        parent::__construct($security);

        $this->config = new Config('Request');

        $this->cleanGet = $this->config->get('AllowGetMethod');
        $this->purifyAll = $this->config->get('PurifyAll', false);

        if($this->cleanGet === true) {
            $_GET = array();
        }

        if($this->purifyAll === true) {
            $this->_purifyAll();
        }
    }

    protected function _purifyAll()
    {
        if(isset($_POST) && !empty($_POST))
            $_POST = $this->security->purify($_POST);

        if(isset($_GET) && !empty($_GET))
            $_GET = $this->security->purify($_GET);
    }

}