<?php

namespace Fwepe\Factory;

use Fwepe\Component\Config\ConfigLocator;

/**
 * Config factory, will get config from either in application or system
 *
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 * @package Fwepe\Factory
 */

class Config
{

    /**
     * Setting up path for $appConfig and $sysConfig
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $name = str_replace("/", DS, $name);

        $appConfigFile = APP_ROOT  .DS. 'config' .DS. $name . '.conf.php';
        $systemConfigFile = SYSTEM_ROOT .DS. 'config' .DS. $name . '.conf.php';
        
        $systemConfig = new ConfigLocator($systemConfigFile);
        $appConfig = new ConfigLocator($appConfigFile);
        
        $systemCfg = $systemConfig->getCfg();
        $appCfg = $appConfig->getCfg();
        
        
        
        $this->cfg = array_merge($systemCfg, $appCfg);
    }

    /**
     * Get the value from key
     *
     * @param string $key
     * @param string $default
     */

    public function get($key, $default = null)
    {
        $value = $default;
        
        if(isset($this->cfg[$key]))
            $value = $this->cfg[$key];

        return $value;

    }
    
    public function getCfg()
    {
        return $this->cfg;
    }
}

/*** End: Config.php ***/
