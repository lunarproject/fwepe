<?php

namespace Fwepe\Factory;

class Autoloader
{
    public static function setPath(array $paths)
    {
        $pathEnv = implode(PATH_SEPARATOR, $paths);
        
        set_include_path(get_include_path() . PATH_SEPARATOR . $pathEnv);
    }
    
    public static function autoload($className)
    {
        if(!class_exists($className))
        {
            $className = ltrim($className, '\\');
            $fileName  = '';
            $namespace = '';
            $lastNsPos = strrpos($className, '\\');
            if ($lastNsPos) {
                $namespace = substr($className, 0, $lastNsPos);
                $className = substr($className, $lastNsPos + 1);
                $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) 
                        . DIRECTORY_SEPARATOR;
            }
            $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

            require($fileName);
        }
    }

    public static function register(array $paths)
    {
        Autoloader::setPath($paths);
        
        $load = array('\\Fwepe\\Factory\\Autoloader', 'autoload');
        if(function_exists('spl_autoload_register'))
            spl_autoload_register($load);
    }
    
    public static function unregister()
    {
        $load = array('\\Fwepe\\Factory\\Autoloader', 'autoload');
        if(function_exists('spl_autoload_unregister'))
            spl_autoload_unregister($load);
    }
}

/*** End: Autoloader.php ***/
