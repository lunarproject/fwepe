<?php

namespace Fwepe\Factory;

class I18n
{
    private static $lang;

    public function __construct()
    {
        $this->config = new Config('Translator');

        $this->_loadEngine();
    }

    private function _loadEngine()
    {
        $engine = $this->config->get('TranslatorEngine');

        $engineClass = '\\Fwepe\\Component\\Language\\' . $engine . 'Translator';

        if(class_exists($engineClass))
            $this->engine = new $engineClass();
        else
            trigger_error("Translator Engine not found! Engine: $engine", E_USER_ERROR);

        $this->engine->setLanguage(self::getLanguage());
    }

    public function translate($text)
    {
        $this->engine->setLanguage(self::getLanguage());
        return $this->engine->translate($text);
    }

    public static function setLanguage($lang)
    {
        self::$lang = $lang;
    }

    public static function getLanguage()
    {
        return self::$lang;
    }

}

/*** End: Translator.php ***/
