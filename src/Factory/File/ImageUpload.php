<?php

namespace Fwepe\Factory\File;

use Fwepe\Component\File\ImageUpload as BaseImageUpload;
use Fwepe\Factory\Config;

class ImageUpload extends BaseImageUpload
{
     public function __construct($file, $location, array $options = array())
    {
        $config = new Config('ImageUpload');
        $this->options = array(
            'max_size'      => $config->get('MaxSize'),
            'permission'    => $config->get('Permission'),
            'location'      => $config->get('Location'),
            'pattern'       => $config->get('Pattern'),
            'counter_digit' => $config->get('CounterDigit'),
            'overwrite'     => $config->get('Overwrite')
        );

        parent::__construct($file, $location, $options);

        $this->setAllowedExtensions($config->get('AllowedExtentions'));
    }
}

/*** End: ImageUpload.php ***/
