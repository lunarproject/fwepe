<?php

namespace Fwepe\Component\Session;

class SessionManager
{
    protected $options = array(
        'name'               => 'FWEPE_SESSION',
        'active_time'        => 300,
        'check_user_agent'   => true,
        'refresh_session_id' => 300,
        'prefix'             => 'fwepe_',
        'token'              => 'fwepe_token'
    );

    public function __construct(array $options = array())
    {
        $this->options = array_merge($this->options, $options);

        $this->options['token'] = md5($this->options['token']);

        session_name($this->options['name']);

        @session_start();

        if($this->get('session_started')) {
            if($this->options['token'] != '')
                $this->_setupToken();

            if($this->options['active_time'] > 0 &&
               $this->options['active_time'] != '' &&
               !$this->get("unlimited_session"))
                $this->_setupActiveTime();

            if($this->options['check_user_agent'] === true)
                $this->_setupUserAgent();

            if($this->options['refresh_session_id'] > 0 &&
                $this->options['refresh_session_id'] != '')
                $this->_setupRefreshId();
        }
    }

    protected function _setupToken() {
        if($this->get('token') !== $this->options['token']) {
            $this->destroy();
        }
    }

    protected function _setupActiveTime()
    {
        $now = time();

        $lastActivity = $this->get("last_activity");

        if($now - $lastActivity > $this->options['active_time']) {
            $this->destroy();
        } else {
            $this->set("last_activity", $now);
        }
    }

    protected function _setupUserAgent()
    {
        if($this->get("user_agent") != $_SERVER["HTTP_USER_AGENT"]) {
            $this->destroy();
        }
    }

    protected function _setupRefreshId()
    {
        $now = time();
        if($this->options['refresh_session_id'] > 0) {
            $lastRefresh = $this->get("last_refresh");
            if($now - $lastRefresh > $this->options['refresh_session_id'] ) {
                session_regenerate_id();
                $this->set("last_refresh", $now);
            }
        }
    }

    protected function _isStarted()
    {
        return isset($_SESSION);
    }
    
    public function setUnlimitedSession($boolean)
    {
		if($boolean)
		{
            $this->set("unlimited_session", true);
		}
		
	}

    public function get($key)
    {
        if(isset($_SESSION[$this->options['prefix'] . $key])) {
            $value = $_SESSION[$this->options['prefix'] . $key];
            return $value;
        } else {
            return null;
        }
    }
    

    public function set($key, $value = true)
    {
        if(isset($_SESSION))
            $_SESSION[$this->options['prefix'] . $key] = $value;
        else
            trigger_error('Session has not started yet', E_USER_WARNING);
    }

    public function start()
    {
        session_regenerate_id();

        $this->set('session_started', true);

        if($this->options['check_user_agent'] === true)
            $this->set('user_agent' , $_SERVER['HTTP_USER_AGENT']);

        $now = time();
        if($this->options['active_time'] > 0 && $this->options['active_time'] != '')
            $this->set('last_activity', $now);

        if($this->options['refresh_session_id'] > 0 && $this->options['refresh_session_id'] != '')
            $this->set('last_refresh', $now);

        $this->set('token', $this->options['token']);
    }

    public function destroy()
    {
        $sessionName = session_name();
        setcookie($sessionName, "", time());
        session_regenerate_id();

        if($this->_isStarted())
            $_SESSION = array();

        @session_destroy();
    }
}

/*** End: SessionManager.php ***/
