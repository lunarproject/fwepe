<?php

namespace Fwepe\Component\Config;

class ConfigLocator
{
    private $file;
    private $cfg = array();
    
    public function __construct($file)
    {
        $this->file = $file;
        if(is_file($this->file)) {
            include($this->file);
            $this->cfg = $cfg;
        }
    }

    public function getCfg()
    {
        return $this->cfg;
    }

    public function hasKey($key)
    {
        if(isset($this->cfg[$key]))
            return true;
        else
            return false;
    }

}

/*** End: ConfigLocator.php ***/
