<?php

namespace Fwepe\Component\Request;

use Fwepe\Component\Security\AbstractSecurity;

class RequestManager
{
    protected $security;
    protected $purify;
    protected $purifyConfig;

    public function __construct(AbstractSecurity $security)
    {
        $this->security = $security;

    }

    protected function _purify($var)
    {
        if($this->purify === true) {
            return $this->security->purify($var, $this->purifyConfig);
        } else {
            return $var;
        }
    }
    protected function _fetch($array, $mixed)
    {
        if(is_array($mixed)) {
            $data = array();
            foreach($mixed as $key => $value) {
                if(is_int($key)) {
                    $data[$value] = $this->_fetch($array, $value);
                } else {
                    $data[$key] = $this->_fetch($array, $value);
                }
            }

            return $data;
        } else {
            if(isset($array[$mixed])) {
                return $this->_purify($array[$mixed]);
            } else {
                return null;
            }
        }
    }

    public function post($key, $purify = false, $config = array()) {
        $this->purify = $purify;
        $this->purifyConfig = $config;
        return $this->_fetch($_POST, $key);
    }

    public function get($key, $purify = false, $config = array()) {
        $this->purify = $purify;
        $this->purifyConfig = $config;
        
        return $this->_fetch($_GET, $key);
    }
}

/*** End: RequestManager.php ***/
