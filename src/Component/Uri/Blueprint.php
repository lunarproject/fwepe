<?php

namespace Fwepe\Component\Uri;

use Fwepe\Exception\RouterException;

/**
 * Class that will build URL routing
 *
 * @author Farly FD <farly[at]lunarproject[dot]org>
 * @package Fwepe\Component\Uri
 */
class Blueprint
{
    protected $_name;
    protected $_route;
    protected $_target;
    protected $_routeRegex;
    protected $_nodes = array();
    
    const PARAMS_PATTERN = '/\{(\d+)(\:(\w+))?\}/i';
    
    public function __construct($route, $target, $name=null)
    {
        //TODO: clean route, target, and name. Check if node index skipped.
        $this->_route = rtrim($route, "/");
        
        if(!$this->_route)
        {
            $this->_route = '/';
        }

        $this->_target = $target;
        
        if(!$name)
            $this->_name = $name;
        else
            $this->_name = $target;
    }
    
    public function getName()
    {
        return $this->_name;
    }
    
    public function getTarget()
    {
        $tmp = explode("::", $this->_target);
        
        return array("class" => $tmp[0], "method" => $tmp[1]);
    }

    public function match($text)
    {
        return preg_match($this->getRouteRegex(), $text);
    }
    
    public function extractPathValues($path)
    {
        $pattern = $this->getRouteRegex();
        
        $values = array();
        $matches = array();
        
        if(preg_match($pattern, $path, $matches))
        {
            unset($matches[0]);
            
            foreach($matches as $match)
            {
                $values[] = $match;
            }
        }
        
        return $values;
    }
    
    public function getParams($path)
    {
        $values = $this->extractPathValues($path);
        $params = array();
        
        foreach($values as $key => $value)
        {
            if(isset($this->_nodes[$key]))
            {
                $paramIndex = $this->_nodes[$key]->getIndex() - 1;
                $params[$paramIndex] = $value;
            }
            else
            {
                throw new RouterException("Offset for $key not found");
            }
        }
        
        ksort($params);
        
        return $params;
    }
    
    public function getRouteRegex()
    {
        if(!$this->_routeRegex)
            $this->_routeRegex = preg_replace_callback(
                    self::PARAMS_PATTERN, array(&$this,
                    '_substitutePattern'), $this->_route);
        
        return "~^{$this->_routeRegex}$~";
    }
    
    public function reverseUrl($params)
    {
        $route = $this->_route;
        
        if(empty($this->_nodes))
        {
            $this->getRouteRegex();
        }
        
        foreach($params as $key => $param)
        {
            foreach($this->_nodes as &$node)
            {
                $this->_setReverseNodeValue($key, $param, $node);
            }
        }
        
        foreach($this->_nodes as &$node)
        {
            $route = str_replace($node->toString(), $node->getValue(), $route);
        }
        
        return $route;
    }
    
    protected function _setReverseNodeValue($key, $value, &$node)
    {
        if(((is_int($key) && ($key + 1) == $node->getIndex())
                || ($node->getName() && $key == $node->getName()))
                && !$node->getValue())
        {
            $node->setValue($value);
        }
    }
    
    protected function _substitutePattern($matches)
    {
        $keys = array();
        if(isset($matches[1]))
        {
            $key = $matches[1];
            
            $node = new RouteNode();
            $node->setIndex($key);

            if(!array_key_exists($key, $keys))
            {
                $keys[$key] = null;
            }
            else
            {
                throw new RouterException("Route index \"$key\" already exist");
            }
            
            if(isset($matches[3]))
            {
                if(!in_array($matches[3], $keys))
                {
                    $keys[$key] = $matches[3];
                    $node->setName($matches[3]);
                }
                else
                {
                    throw new RouterException("Route name \"{$matches[3]}\" "
                    . "already exists");
                }
            }
            
            $this->_nodes[] =& $node;
        }
        
        $validRegex = "([\w-]+)";
        return $validRegex;
    }
    
    
}
