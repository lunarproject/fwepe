<?php

namespace Fwepe\Component\Uri;

use Fwepe\Component\Uri\Blueprint;

class Router
{
    const TOKEN_PATTERN = '/\{(\d+)?(:\w+)\}/i';
    
    protected $routes = array();

    public function __construct($routes)
    {
        $this->routes = $routes;
    }
    
    public function getBlueprint($path)
    {
        foreach ($this->routes as $route)
        {
            $pattern = $this->_parseRouteToRegexPattern($route[0]);
            
            $matches = array();
            
            if(preg_match($pattern, $path, $matches))
            {
                $params = $this->_setupParams($route, $matches);
                
                break;
                
            }
        }
        
    }
    
    protected function _setupParams($route, $matches)
    {

        preg_match_all(self::TOKEN_PATTERN, $route, $fMatches);
        
        $params = array();
        
        $index = 1;
                
        foreach($fMatches as $fMatch)
        {
            $params[$fMatch[1]] = $matches[$index];
            $index++;
        }
        
        return $params;
    }
    
    protected function _parseRouteToRegexPattern($route)
    {
        $replacement = "(\w+)";
        
        $parsedRoute = preg_replace(self::TOKEN_PATTERN, $replacement, $route);
        
        return $parsedRoute;
    }
}

/*** End: Router.php ***/
