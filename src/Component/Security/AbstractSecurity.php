<?php

namespace Fwepe\Component\Security;

abstract class AbstractSecurity
{
    abstract public function purify($text, $config = array());

}

/*** End: AbstractSecurity.php ***/
