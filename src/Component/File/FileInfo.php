<?php

namespace Fwepe\Component\File;

/**
 * This is a simple FileInfo class which doesn't use PHP fileinfo extention
 *
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 * @package Fwepe\Component\File
 */

class FileInfo
{
    /**
     * File name with extention
     *
     * @var string $file
     */
    protected $file;

    /**
     * file location
     *
     * @var string $location
     */
    protected $location;

    /**
     * MIME type of the file
     *
     * @var string $mimeType
     */
    protected $mimeType;

    /**
     * File size
     *
     * @var int $size
     */
    protected $size;

    /**
     * File's name without extention
     *
     * @var string $name
     */
    protected $name;

    /**
     * FileInfo contructor
     *
     * @param string $file
     * @param string $location
     */
    public function __construct($file, $location)
    {
        $location = str_replace('/', DS, $location);
        $location = str_replace('\\', DS, $location);
        $location = rtrim($location, DS);
        $this->file = basename($file);
        $this->location = $location;
    }

    /**
     * Get file name with extention
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }
    /**
     * Get file name without extention
     *
     * @return string
     */
    public function getName()
    {
        if(empty($this->name)) {
            $array = explode(".", $this->getFile());
            array_pop($array);
            $this->name = implode(".", $array);
        }

        return $this->name;
    }

    /**
     * Set file name without extention
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get file's extention
     *
     * @return string
     */
    public function getExtention()
    {
        $file = explode('.', $this->getFile());
        return strtolower(end($file));
    }

    /**
     * Get file's MIME type
     *
     * @return string
     */
    public function getMimeType()
    {
        if(empty($mimeType)) {

            $this->mimeType = 'application/unknown';

            $mimeFile = dirname(__FILE__) .DS. 'mime.types';

            if(is_file($mimeFile)) {
                $mimeFileContent = file_get_contents($mimeFile);

                $fileExtention = $this->getExtention();
                $pattern = '/^([\w\+\-\.\/]+)\s+(\w\s*)*(' . $fileExtention . '\s*)(\w\s*)*$/im';
                if(preg_match($pattern, $mimeFileContent, $match))
                    $this->mimeType = $match[1];
            }
        }

        return $this->mimeType;
    }

    /**
     * Get file's size
     *
     * @return int
     */
    public function getSize()
    {
        if(empty($this->size)) {
            $this->size = filesize($this->location .DS. $this->file);
        }

        return $this->size;
    }
}


/*** End: FileInfo.php ***/
