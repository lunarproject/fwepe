<?php

namespace Fwepe\Component\File;

interface Executable
{
    public function prepare();
    public function isReady();
    public function execute();
}

/*** End: Executable.php ***/
