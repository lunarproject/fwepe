<?php

namespace Fwepe\Component\File;

/**
 * This is a simple way to manage file uploading
 *
 * TODO: Clean file renaming code!
 *
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 * @package Fwepe\Component\File
 */
class Upload implements Executable
{
    /**
     * Upload options
     *
     * @var array
     */
    protected $options = array(
        'max_size'      => 512000,
        'permission'    => 0775,
        'pattern'       => ':name:_:counter:',
        'counter_digit' => 2,
        'overwrite'     => false
    );

    /**
     * File from HTTP Upload this equals to $_FILES[<name>]
     *
     * @var array
     */
    protected $file;

    /**
     * Location which the file will be uploaded
     *
     * @var string
     */
    protected $location;

    /**
     * Allowed extentions to be uploaded
     *
     * @var array
     */
    protected $allowedExtensions = array(
        //Archive
        'zip', '7z', 'rar',
        //Document
        'txt', 'pdf', 'doc', 'xls', 'ppt',
        //Image
        'gif', 'png', 'jpg', 'jpeg',
        //Audio
        'mp3', 'wav', 'mpeg', 'mpg', 'mpe', 'ogg',
        //Video
        'mov', 'avi'
    );

    /**
     * The name of file after uploaded
     *
     * @var string
     */
    protected $name = '';

    /**
     * File's upload status
     *
     * @var array
     */
    public $status = array(
        'upload'  => false,
        'error'   => -1,
        'message' => ''
    );

    /**
     * Upload's contructor
     *
     * @param string $file
     * @param string $location
     * @param array $options
     */
    public function __construct($file, $location, array $options = array())
    {
        $this->file     = $file;
        $this->location = $location;
        $this->setOptions($options);
    }

    /**
     * Set upload's options
     *
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = array_merge($this->options, $options);
    }

    /**
     * Set upload's allowed extentions
     *
     * @param array $allow
     */
    public function setAllowedExtensions($allow)
    {
        if(is_array($allow)) {
            $this->allowedExtensions = $allow;
        } else {
            $this->allowedExtensions = (array) $allow;
        }
    }

    /**
     * Set file's name after uploaded
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Set uploaded max size
     *
     * @param int $size
     */
    public function setMaxSize($size)
    {
        $this->maxSize = $size;
    }

    /**
     * Get uploaded file name with extention
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->getName() . "." . $this->getExtension();
    }

    /**
     * Get file's type
     *
     * @return string
     */
    public function getType()
    {
        return $this->file["type"];
    }

    /**
     * Get file's extention
     *
     * @return string
     */
    public function getExtension()
    {
        if(empty($this->extension)) {
            $array = explode(".", $this->file["name"]);
            $this->extension = strtolower(end($array));
        }
        return $this->extension;
    }

    /**
     * Get file's name without extention
     *
     * @return string
     */
    public function getName()
    {
        if(empty($this->name)) {
            $array = explode(".", $this->file["name"]);
            array_pop($array);
            $this->name = implode(".", $array);
        }

        return $this->name;
    }

    /**
     * Get file from HTTP Upload
     *
     * @return array
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Prepare the file to be uploaded
     *
     * @return boolean
     */
    public function prepare()
    {
        extract($this->options);

        if ($this->file['error'] > 0 && $this->file['error'] < 10)
        {
            $this->status['error'] = $this->file['error'];
            switch($this->file['error']) {
                case 1:
                    $this->status['message'] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case 2:
                    $this->status['message'] = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case 3:
                    $this->status['message'] = 'The uploaded file was only partially uploaded';
                    break;
                case 4:
                    $this->status['message'] = 'No file was uploaded';
                    break;
                case 6:
                    $this->status['message'] = 'Missing a temporary folder';
                    break;
                case 7:
                    $this->status['message'] = 'Failed to write file to disk';
                    break;
                case 8:
                    $this->status['message'] = 'A PHP extension stopped the file upload';
                    break;
            }
            return false;
        }

        if (!in_array($this->getExtension(), $this->allowedExtensions)) {
            $this->status['error'] = 10;
            $this->status['message'] = 'File type is not allowed!';
            return false;
        }

        if($this->file['size'] > $max_size) {
            $this->status['error'] = 11;
            $this->status['message'] = sprintf('The uploaded file exceeds max file size: %d Byte', $max_size);
            return false;
        }

        $this->location = rtrim($this->location, '/');
        $this->location = rtrim($this->location, '\\');
        $this->location = rtrim($this->location, DS);
        if(!is_dir($this->location)) {
            $locationStatus = mkdir($this->location, $permission, true);
            chmod($this->location, $permission);
            if($locationStatus === false) {
                $this->status['error'] = 12;
                $this->status['message'] = 'Cannot reach upload location. Cannot move file';
                return false;
            }
        }

        if(!is_writable($this->location)) {
            $this->status['error'] = 13;
            $this->status['message'] = 'Uploaded file location is not writable. Cannot move file';
            return false;
        }

        if(!is_uploaded_file($this->file['tmp_name'])) {
            $this->status['error'] = 14;
            $this->status['message'] = 'File not uploaded via HTTP. Possible file upload attack';
            return false;
        }


        if(!is_file($this->location .DS. 'index.html')) {
            fclose(fopen($this->location .DS. 'index.html', 'w'));
            @chmod($this->location .DS. 'index.html', $permission);
        }

        $this->status['upload'] = true;
        $this->status['error']  = 0;
        $this->status['message'] = 'File is ready to upload';
        
        return true;
    }
    
    /**
     * Get file size
     * 
     * @return int
     */
    public function getSize()
    {
		return $this->file['size'];
	}

    /**
     * Check if file's is ready to move
     *
     * @return boolean
     */
    public function isReady()
    {
        return $this->status['upload'];
    }

    /**
     * Move the uploaded file
     *
     * @return boolean
     */
    public function execute()
    {
        if(!$this->isReady()) {
            trigger_error('File is not ready to upload', E_USER_WARNING);
            return false;
        }

		if($this->options['overwrite'] === false)
			$this->_setupFileName();

        $fileName = $this->getFileName();
        move_uploaded_file($this->file['tmp_name'], $this->location .DS. $fileName);
        @chmod($this->location .DS. $fileName, $this->options['permission']);

        $this->status['message'] = sprintf('Success uploading file: "%s"', $fileName);

        return true;
    }

    /**
     * Setup the file's name if file's name already exist in location
     */
    protected function _setupFileName()
    {
        $finalName = $originalName = $this->getName();
        $counter = 1;

        $name = $originalName . '.' . $this->getExtension();

        while(is_file($this->location .DS. $name)) {
            $counterName = sprintf("%0" . $this->options['counter_digit'] . "u", $counter++);
            $name = $this->options['pattern'];
            $name = str_replace(":counter:", $counterName, $name);
            $name = str_replace(":name:", $originalName, $name);
            $finalName = $name;
            $name .= "." . $this->getExtension();
        }

        $this->setName($finalName);
    }
}

/*** End: Upload.php ***/
