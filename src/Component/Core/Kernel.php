<?php

namespace Fwepe\Component\Core;

/**
 * The kernel
 *
 * @package Fwepe\Core
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 */

class Kernel
{
    
    protected $class;
    
    protected $method;
    
    protected $params;
   
    /**
     * kernel constructor
     */
    public function __construct($class, $method, $params)
    {        
        $this->class = $class;
        $this->method = $method;
        $this->params = $params;
        
    }

    /**
     * Build the controller
     *
     * @return boolean
     */
    public function build()
    {
       
        $instance = new $this->class();
        
        return call_user_func_array(
                array($instance, $this->method),
                $this->params
        );
    }

}

/*** End: Kernel.php ***/
