<?php

namespace Fwepe\Component\Language;

/**
 * Engine translator
 *
 * @author Farly Fitrian Dwiputra <farly@lunarproject.org>
 * @package Fwepe\Language
 */

abstract class AbstractTranslator
{
    abstract public function setLanguage($lang);

    abstract public function translate($text);
}

/*** End: AbstractTranslator.php ***/
