<?php

namespace Fwepe\Component\Language;

class DummyTranslator extends AbstractTranslator
{
    public function __construct()
    {
        parent::__construct();
        if (!is_writable(PATH_TMP))
        {
            trigger_error('PATH_TMP is not writable!', E_USER_ERROR);
        }
        $this->file     = MY_APP . '.txt';
        $this->location = PATH_TMP .DS. 'DummyTranslator' . DS;
    }

    public function setLanguage($locale)
    {
        return;
    }

    private function _checkFileContents($text)
    {
        $file = $this->location . $this->file;
        if(is_file($file)) {
            $tmp = file_get_contents($file);
            $fileContents = explode("\r\n" . str_repeat('=', 30) . "\r\n", $tmp);

            if (preg_grep('/^' . $text . '$/', $fileContents))
            {
                return true;
            }
        } else {
            if(!is_dir($this->location)) {
                mkdir($this->location, 0777, true);
                chmod($this->location, 0777);
            }
        }

        return false;
    }

    public function translate($text)
    {
        $status = $this->_checkFileContents($text);

        if(!$status) {
            $fileHandler = fopen($this->location . $this->file, 'a+');
            $addText = $text . "\r\n" . str_repeat('=', 30) . "\r\n";
            $status = fwrite($fileHandler, $addText);
            fclose($fileHandler);
            if (!is_writable($this->location . $this->file))
            {
                chmod($this->location . $this->file, 0777);
            }
        }
        
        if (is_int($status))
        {
            return "DummyTranslator succeed to list this text: $text";
        }
        else if ($status === true)
        {
            return "DummyTranslator found this text: '$text' on the list ";
        }
        else if ($status === false)
        {
            return "DummyTranslator failed to list this text: $text";
        }
    }

}


/*** End: DummyTranslator.php ***/
