<?php

namespace Fwepe\Component\Plugin;

abstract class ControllerPlugin extends BasePlugin
{

    abstract public function onConstruct();


}
